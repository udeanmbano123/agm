namespace AGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial19 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Registers", "Balance", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Registers", "Balance", c => c.Double(nullable: false));
        }
    }
}
