namespace AGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial12 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Contents", "AccountID", "dbo.User");
            DropIndex("dbo.Contents", new[] { "AccountID" });
            AddColumn("dbo.Contents", "User_UserId", c => c.Int());
            CreateIndex("dbo.Contents", "User_UserId");
            AddForeignKey("dbo.Contents", "User_UserId", "dbo.User", "UserId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Contents", "User_UserId", "dbo.User");
            DropIndex("dbo.Contents", new[] { "User_UserId" });
            DropColumn("dbo.Contents", "User_UserId");
            CreateIndex("dbo.Contents", "AccountID");
            AddForeignKey("dbo.Contents", "AccountID", "dbo.User", "UserId", cascadeDelete: true);
        }
    }
}
