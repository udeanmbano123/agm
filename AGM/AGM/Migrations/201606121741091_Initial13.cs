namespace AGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial13 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Contents", "User_UserId", "dbo.User");
            DropIndex("dbo.Contents", new[] { "User_UserId" });
            DropColumn("dbo.Contents", "AccountID");
            RenameColumn(table: "dbo.Contents", name: "User_UserId", newName: "AccountID");
            AlterColumn("dbo.Contents", "AccountID", c => c.Int(nullable: false));
            CreateIndex("dbo.Contents", "AccountID");
            AddForeignKey("dbo.Contents", "AccountID", "dbo.User", "UserId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Contents", "AccountID", "dbo.User");
            DropIndex("dbo.Contents", new[] { "AccountID" });
            AlterColumn("dbo.Contents", "AccountID", c => c.Int());
            RenameColumn(table: "dbo.Contents", name: "AccountID", newName: "User_UserId");
            AddColumn("dbo.Contents", "AccountID", c => c.Int(nullable: false));
            CreateIndex("dbo.Contents", "User_UserId");
            AddForeignKey("dbo.Contents", "User_UserId", "dbo.User", "UserId");
        }
    }
}
