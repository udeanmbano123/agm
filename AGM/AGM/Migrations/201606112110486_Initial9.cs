namespace AGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial9 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.AGMs", "Memberno");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AGMs", "Memberno", c => c.String());
        }
    }
}
