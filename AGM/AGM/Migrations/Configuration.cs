using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using AGM.Models;

namespace AGM.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<AGM.DAL.DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(AGM.DAL.DataContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
         /*  Role role1 = new Role { RoleName = "Admin" };
            Role role2 = new Role { RoleName = "User" };


            User user1 = new User { Username = "admin", Email = "admin@agm.com", FirstName = "Admin",LastName="Cocu", Password = ComputeHash("123456", new SHA256CryptoServiceProvider()), ConfirmPassword = ComputeHash("123456", new SHA256CryptoServiceProvider()), IsActive = true, IDNo = "23", Department = "ESCROW SYSTEMS", CreateDate = DateTime.UtcNow, Roles = new List<Role>() };

            User user2 = new User { Username = "user1", Email = "user1@agm.com", FirstName = "User1", LastName = "Noku", Password = ComputeHash("123456", new SHA256CryptoServiceProvider()), ConfirmPassword = ComputeHash("123456", new SHA256CryptoServiceProvider()), IsActive = true, IDNo = "23", Department = "ESCROW SYSTEMS", CreateDate = DateTime.UtcNow, Roles = new List<Role>() };

           user1.Roles.Add(role1);
            user2.Roles.Add(role2);


            context.Users.Add(user1);
            context.Users.Add(user2);
         */
        }
        public string ComputeHash(string input, HashAlgorithm algorithm)
        {
            Byte[] inputBytes = Encoding.UTF8.GetBytes(input);

            Byte[] hashedBytes = algorithm.ComputeHash(inputBytes);

            return BitConverter.ToString(hashedBytes);
        }
    }
}
