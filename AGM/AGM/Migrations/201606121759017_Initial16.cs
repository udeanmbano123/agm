namespace AGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial16 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Contents", "UserID", "dbo.User");
            DropIndex("dbo.Contents", new[] { "UserID" });
            DropTable("dbo.Contents");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Contents",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Image = c.Binary(),
                        UserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateIndex("dbo.Contents", "UserID");
            AddForeignKey("dbo.Contents", "UserID", "dbo.User", "UserId", cascadeDelete: true);
        }
    }
}
