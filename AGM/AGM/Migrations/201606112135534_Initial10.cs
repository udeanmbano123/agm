namespace AGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial10 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AGMs", "agmtype", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AGMs", "agmtype");
        }
    }
}
