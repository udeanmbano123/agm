namespace AGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AGMs", "Description", c => c.String(nullable: false));
            AddColumn("dbo.AGMs", "Venue", c => c.String(nullable: false));
            AddColumn("dbo.AGMs", "StartTime", c => c.String(nullable: false));
            AddColumn("dbo.AGMs", "EndTime", c => c.String(nullable: false));
            AddColumn("dbo.AGMs", "StartDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.AGMs", "EndDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AGMs", "EndDate");
            DropColumn("dbo.AGMs", "StartDate");
            DropColumn("dbo.AGMs", "EndTime");
            DropColumn("dbo.AGMs", "StartTime");
            DropColumn("dbo.AGMs", "Venue");
            DropColumn("dbo.AGMs", "Description");
        }
    }
}
