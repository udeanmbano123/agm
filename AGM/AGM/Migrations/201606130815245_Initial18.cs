namespace AGM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial18 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Registers", "address", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Registers", "address");
        }
    }
}
