﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AGM.DAL.Security;

namespace AGM.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        // [CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
        // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
        [CustomAuthorize(Roles = "Admin", NotifyUrl = "/UnauthorizedPage")]
        public ActionResult Index()
        {
            return View();
        }
    }
}