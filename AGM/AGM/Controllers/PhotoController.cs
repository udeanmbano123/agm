﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AGM.DAL;
using AGM.DAL.Security;
using AGM.Models;
using AGM.Repositories;

namespace AGM.Controllers
{
    [CustomAuthorize(Roles = "Admin", NotifyUrl = "/UnauthorizedPage")]
    public class PhotoController : Controller
    {
       private DataContext db = new DataContext();
        // POST: Accounts/Delete/5
     
        public async Task<ActionResult> DeleteImage(int id)
        {
            Models.Content content = await db.Contents.FindAsync(id);
            db.Contents.Remove(content);
            await db.SaveChangesAsync();
            return RedirectToAction("CreateImage");
        }

        //uploading images
        [Route("Index")]
        [HttpGet]
        public ActionResult ImageIndex()
        {
            System.Web.HttpContext.Current.Session["_SessionCompany"] = "Session works";
            var content = db.Contents.Select(s => new
            {
                s.ID,
                s.Title,
                s.Image,
                s.UserID
            });
            string name = User.Identity.Name;
            var user = db.Users.ToList().Where(a => a.Email == name);
            int? userid = 0;
            foreach (var row in user)
            {
                userid = row.UserId;
            }
            List<ContentViewModel> contentModel = content.Select(item => new ContentViewModel()
            {
                ID = item.ID,
                Title = item.Title,
                Image = item.Image,
               UserID = item.UserID

            }).Where(a => a.UserID == userid).ToList();
            if (contentModel.Count == 0)
            {
                return View("CreateImage");
            }
            else
            {
                return View(contentModel);
            }

        }

        /// <summary>
        /// Retrive Image from database 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult RetrieveImage(int id)
        {
            byte[] cover = GetImageFromDataBase(id);
            if (cover != null)
            {
                return File(cover, "image/jpg");
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public byte[] GetImageFromDataBase(int Id)
        {
            var q = from temp in db.Contents where temp.ID == Id select temp.Image;
            try
            {
                byte[] cover = q.First();
                return cover;
            }
            catch (Exception)
            {
                byte[] cover = null;
                return cover;
            }


        }

        [HttpGet]
        public ActionResult CreateImage()
        {
            string name = User.Identity.Name;
            var user = db.Users.ToList().Where(a => a.Email == name);
            int? userid = 0;
            int count = 0;
            foreach (var row in user)
            {
                userid = row.UserId;
            }
            try
            {
                var swazi = db.Contents.ToList().Where(a => a.UserID == userid);
                foreach (var row in swazi)
                {
                    count++;
                }
                if (count >= 1)
                {
                    return RedirectToAction("ImageIndex");
                }
            }
            catch (Exception e)
            {
                
            }
           
            return View();
        }
        /// <summary>
        /// Save content and images
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("CreateImage")]
        [HttpPost]
        public ActionResult CreateImage(ContentViewModel model)
        {
            string name = User.Identity.Name;
            var user = db.Users.ToList().Where(a => a.Email == name);
            int? userid = 0;
            foreach (var row in user)
            {
                userid = row.UserId;
            }
            HttpPostedFileBase file = Request.Files["ImageData"];
            ContentRepository service = new ContentRepository();
            int id = Convert.ToInt32(userid);
            int i = service.UploadImageInDataBase(file, model, id);
            if (i == 1)
            {
                return RedirectToAction("ImageIndex");
            }
            return View(model);
        }
    }
}