﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AGM.DAL;
using AGM.DAL.Security;
using AGM.Models;
using Database = WebMatrix.Data.Database;
namespace AGM.Controllers
{    // [CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
     // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "Admin", NotifyUrl = "/UnauthorizedPage")]
    public class CompaniesController : Controller
    {
        private DataContext db = new DataContext();

        // GET: Companies
        public async Task<ActionResult> Index()
        {
            return View(await db.Companys.ToListAsync());
        }

        // GET: Companies/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Company company = await db.Companys.FindAsync(id);
            if (company == null)
            {
                return HttpNotFound();
            }
            return View(company);
        }

        // GET: Companies/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Companies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "CompanyID,CompanyCode,CompanyName,ISIN,CompanyClassification,Address,Telephone,Mobile,Email,DateAdded,Website,ActivationStatus")] Company company)
        {
            company.DateAdded=DateTime.Now;
            company.CompanyCode = "A";
            int?id=0;
            var con2 = Database.Open("DataContext");
            var audit = "select max(CompanyID) as ID from Companies";
            var list13 = con2.Query(audit).ToList();
            int? code25 = 0;
            foreach (var row in list13)
            {
                code25 = row.ID;
            }
            if (ModelState.IsValid)
            { 
                string rand = CreateRandomPassword((5));
                string concode25 = Convert.ToString(code25+1);
                string cdsNo = concode25.PadLeft(10, '0')+rand+ company.CompanyName;
                company.CompanyCode = cdsNo;
                db.Companys.Add(company);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(company);
        }

        // GET: Companies/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Company company = await db.Companys.FindAsync(id);
            if (company == null)
            {
                return HttpNotFound();
            }

            return View(company);
        }

        // POST: Companies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "CompanyID,CompanyCode,CompanyName,ISIN,CompanyClassification,Address,Telephone,Mobile,Email,DateAdded,Website,ActivationStatus")] Company company)
        {
            if (ModelState.IsValid)
            {
                db.Entry(company).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(company);
        }

        // GET: Companies/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Company company = await db.Companys.FindAsync(id);
            if (company == null)
            {
                return HttpNotFound();
            }
            return View(company);
        }

        // POST: Companies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Company company = await db.Companys.FindAsync(id);
            db.Companys.Remove(company);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public string CreateRandomPassword(int size)
        {
            string allowedChars = "";
            allowedChars += "1,2,3,4,5,6,7,8,9,0";
            char[] sep = { ',' };
            string[] arr = allowedChars.Split(sep);
            string passwordString = "";
            string password = "";
            string temp = "";
            Random rand = new Random();
            for (int i = 0; i < size; i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                passwordString += temp;
            }
            password = passwordString;
            return password;
        }
    }
}
