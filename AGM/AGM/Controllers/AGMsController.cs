﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AGM.DAL;
using AGM.DAL.Security;
using AGM.Models;

namespace AGM.Controllers
{   // GET: Admin
    // [CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
    // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "Admin", NotifyUrl = "/UnauthorizedPage")]
    public class AGMsController : Controller
    {
        private DataContext db = new DataContext();

        // GET: AGMs
        public async Task<ActionResult> Index()
        {
            int companies = db.Companys.Where(a => a.ActivationStatus == true).ToList().Count();

            if (companies <= 0)
            {
                System.Web.HttpContext.Current.Session["_CompanyID"] = "No companies are activated";
                System.Web.HttpContext.Current.Session["_CompanyName"] = "";
            }
            else if (companies > 1)
            {
                System.Web.HttpContext.Current.Session["_CompanyID"] = companies;
                System.Web.HttpContext.Current.Session["_CompanyName"] = "Only one company can be activated";
            }
            else
            {
                System.Web.HttpContext.Current.Session["_CompanyID"] = companies;
                System.Web.HttpContext.Current.Session["_CompanyName"] = "";
                var comp = db.Companys.ToList().Where(a => a.ActivationStatus == true);

                foreach (var row in comp)
                {
                    System.Web.HttpContext.Current.Session["_CompanyName"] = row.CompanyName;
                    System.Web.HttpContext.Current.Session["_CompanyRegID"] = row.CompanyID;
                }
            }
            var aGMs = db.AGMs.Include(a => a.Company);
            return View(await aGMs.ToListAsync());
        }

        // GET: AGMs/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Models.AGM aGM = await db.AGMs.FindAsync(id);
            if (aGM == null)
            {
                return HttpNotFound();
            }
            return View(aGM);
        }

        // GET: AGMs/Create
        public ActionResult Create()
        {
            int companies = db.Companys.Where(a => a.ActivationStatus == true).ToList().Count();

            if (companies <= 0)
            {
                System.Web.HttpContext.Current.Session["_CompanyID"] = "No companies are activated";
                System.Web.HttpContext.Current.Session["_CompanyName"] = "";
                return RedirectToAction("Index");
            }
            else if (companies > 1)
            {
                System.Web.HttpContext.Current.Session["_CompanyID"] = companies;
                System.Web.HttpContext.Current.Session["_CompanyName"] = "Only one company can be activated";
                return RedirectToAction("Index");
            }
            else
            {
                System.Web.HttpContext.Current.Session["_CompanyID"] = companies;
                System.Web.HttpContext.Current.Session["_CompanyName"] = "";
                var comp = db.Companys.ToList().Where(a => a.ActivationStatus == true);

                foreach (var row in comp)
                {
                    System.Web.HttpContext.Current.Session["_CompanyName"] = row.CompanyName;
                    System.Web.HttpContext.Current.Session["_CompanyRegID"] = row.CompanyID;
                }
            }
            ViewBag.CompanyID = new SelectList(db.Companys, "CompanyID", "CompanyCode");
            return View();
        }

        // POST: AGMs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "AGMID,AGMstatus,CompanyID,Description,Venue,agmtype,StartTime,EndTime,StartDate,EndDate,DateAdded")] Models.AGM aGM)
        {
            int companies = db.Companys.Where(a => a.ActivationStatus == true).ToList().Count();

            if (companies <= 0)
            {
                System.Web.HttpContext.Current.Session["_CompanyID"] = "No companies are activated";
                System.Web.HttpContext.Current.Session["_CompanyName"] = "";
                return RedirectToAction("Index");
            }
            else if(companies > 1)
            {
                System.Web.HttpContext.Current.Session["_CompanyID"] = companies;
                System.Web.HttpContext.Current.Session["_CompanyName"] = "Only one company can be activated";
                return RedirectToAction("Index");
            }
            else
            {
                System.Web.HttpContext.Current.Session["_CompanyID"] = companies;
                System.Web.HttpContext.Current.Session["_CompanyName"] = "";
                var comp= db.Companys.ToList().Where(a => a.ActivationStatus == true);

                foreach (var row in comp)
                {
                    System.Web.HttpContext.Current.Session["_CompanyName"] = row.CompanyName;
                    aGM.CompanyID= row.CompanyID;
                    System.Web.HttpContext.Current.Session["_CompanyRegID"] = row.CompanyID;
                }
            }
       
            aGM.DateAdded=DateTime.Now;
            if (ModelState.IsValid)
            {
                db.AGMs.Add(aGM);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.CompanyID = new SelectList(db.Companys, "CompanyID", "CompanyCode", aGM.CompanyID);
            return View(aGM);
        }

        // GET: AGMs/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Models.AGM aGM = await db.AGMs.FindAsync(id);
            if (aGM == null)
            {
                return HttpNotFound();
            }
            ViewBag.CompanyID = new SelectList(db.Companys, "CompanyID", "CompanyCode", aGM.CompanyID);

            return View(aGM);
        }

        // POST: AGMs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "AGMID,AGMstatus,CompanyID,Description,Venue,agmtype,StartTime,EndTime,StartDate,EndDate,DateAdded")] Models.AGM aGM)
        {
            if (ModelState.IsValid)
            {
                db.Entry(aGM).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.CompanyID = new SelectList(db.Companys, "CompanyID", "CompanyCode", aGM.CompanyID);
            return View(aGM);
        }

        // GET: AGMs/Edit/5
        public async Task<ActionResult> SEdit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Models.AGM aGM = await db.AGMs.FindAsync(id);
            if (aGM == null)
            {
                return HttpNotFound();
            }
            System.Web.HttpContext.Current.Session["_AGMID"]=id;
            ViewBag.CompanyID = new SelectList(db.Companys, "CompanyID", "CompanyCode", aGM.CompanyID);
            ViewBag.StartDate = aGM.StartDate;
            ViewBag.EndDate = aGM.EndDate;
            return View(aGM);
        }

        // POST: AGMs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SEdit([Bind(Include = "AGMID,AGMstatus,CompanyID,Description,Venue,agmtype,StartTime,EndTime,StartDate,EndDate,DateAdded")] Models.AGM aGM)
        {
            if (ModelState.IsValid)
            {
                db.Entry(aGM).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.CompanyID = new SelectList(db.Companys, "CompanyID", "CompanyCode", aGM.CompanyID);
            return View(aGM);
        }

        // GET: AGMs/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Models.AGM aGM = await db.AGMs.FindAsync(id);
            if (aGM == null)
            {
                return HttpNotFound();
            }
            return View(aGM);
        }

        // POST: AGMs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Models.AGM aGM = await db.AGMs.FindAsync(id);
            db.AGMs.Remove(aGM);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
