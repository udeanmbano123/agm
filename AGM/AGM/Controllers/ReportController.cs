﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AGM.DAL;
using AGM.Models;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace AGM.Controllers
{
    public class ReportController : Controller
    {
        // GET: Report
        private DataContext db = new DataContext();

        public ActionResult RegisterReport()
        {
            var list = db.Companys.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist = new List<SelectListItem>();
            selectlist.Add(new SelectListItem() { Text = "", Value = "" });
            foreach (var row in list)
            {
                //Adding every record to list  
                selectlist.Add(new SelectListItem { Text = row.CompanyName, Value = row.CompanyName.ToString() });
            }
            var list2 = db.AGMs.ToList().Where(a => a.CompanyID == 0);
            //Create List of SelectListItem
            List<SelectListItem> selectlist2 = new List<SelectListItem>();
            selectlist2.Add(new SelectListItem() { Text = Request[""], Value = Request[""] });
            foreach (var row in list2)
            {
                //Adding every record to list  
                selectlist2.Add(new SelectListItem { Text = row.AGMID.ToString(), Value = row.AGMID.ToString() });
            }

            ViewBag.Category = selectlist2;
            ViewBag.Role = selectlist;
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RegisterReport([Bind(Include = "CompanyName,agmID")] RegisterReport registerreport)
        {
            if (ModelState.IsValid)
            {
              System.Web.HttpContext.Current.Session["SearchID"]= Request["CompanyName"];
                System.Web.HttpContext.Current.Session["SearchID2"] = Request["agmID"];
                return Redirect("~/Reporting/RegisterReport.aspx");
            }
            var list = db.Companys.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist = new List<SelectListItem>();
            selectlist.Add(new SelectListItem() { Text = Request["CompanyName"], Value = Request["CompanyName"] });
            foreach (var row in list)
            {
                //Adding every record to list  
                selectlist.Add(new SelectListItem { Text = row.CompanyName, Value = row.CompanyName.ToString() });
            }
            var list2 = db.Companys.ToList().Where(a => a.CompanyName == Request["CompanyName"]);
            int? id=0;
            foreach (var row in list2)
            {
                id = row.CompanyID;
            }

            var list3 = db.AGMs.ToList().Where(a => a.CompanyID == id);
            //Create List of SelectListItem
            List < SelectListItem > selectlist2 = new List<SelectListItem>();
            selectlist2.Add(new SelectListItem() { Text = Request["agmID"], Value = Request["agmID"] });
            foreach (var row in list3)
            {
                //Adding every record to list  
                selectlist2.Add(new SelectListItem { Text = row.AGMID.ToString(), Value = row.AGMID.ToString() });
            }

            ViewBag.Category = selectlist2;
            ViewBag.Role = selectlist;
            return View(registerreport);
        }


    }
}