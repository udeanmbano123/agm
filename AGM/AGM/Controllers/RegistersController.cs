﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using AGM.DAL;
using AGM.Models;
using PagedList;
using Database = WebMatrix.Data.Database;

namespace AGM.Controllers
{
    public class RegistersController : Controller
    {
        private DataContext db = new DataContext();
      public static string file = "";
        // GET: Registers
        public async Task<ActionResult> Index(string sortOrder, string currentFilter,string searchString, int? page)
        {
            var comp = db.Companys.ToList().Where(a => a.ActivationStatus == true);
            int? compID = 0;
            foreach (var row in comp)
            {
                System.Web.HttpContext.Current.Session["_CompanyName"] = row.CompanyName;
                compID = row.CompanyID;
            }
         
            var registers = db.Registers.Include(r => r.AGM).Include(r => r.Company).Where(a=>a.CompanyID==compID);
            if (!String.IsNullOrEmpty(searchString))
            {
                registers = registers.Where(s =>
                s.surnamecompany.ToUpper().Contains(searchString.ToUpper())
                ||
                s.surnamecompany.ToUpper().Contains(searchString.ToUpper()));
            }
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            switch (sortOrder)
            {
                case "name_desc":
                    registers = registers.OrderByDescending(s => s.surnamecompany);
                    break;
                case "Date":
                    registers = registers.OrderBy(s => s.DateAdded);
                    break;
                case "date_desc":
                    registers = registers.OrderByDescending(s => s.DateAdded);
                    break;
                default:
                    registers = registers.OrderBy(s => s.RegisterID);
                    break;
            }
            int pageSize =20;
            int pageNumber = (page ?? 1);
            return View(registers.ToPagedList(pageNumber, pageSize));
       }

        // GET: Registers/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Register register = await db.Registers.FindAsync(id);
            if (register == null)
            {
                return HttpNotFound();
            }
            return View(register);
        }

      
        // GET: Registers/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Register register = await db.Registers.FindAsync(id);
            if (register == null)
            {
                return HttpNotFound();
            }
            ViewBag.AGMID = new SelectList(db.AGMs, "AGMID", "AGMstatus", register.AGMID);
            ViewBag.CompanyID = new SelectList(db.Companys, "CompanyID", "CompanyCode", register.CompanyID);
            return View(register);
        }

        // POST: Registers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "RegisterID,memberno,regstatus,membertype,forenames,surnamecompany,Balance,DateAdded,CompanyID,AGMID,address")] Register register)
        {
            if (ModelState.IsValid)
            {
                db.Entry(register).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.AGMID = new SelectList(db.AGMs, "AGMID", "AGMstatus", register.AGMID);
            ViewBag.CompanyID = new SelectList(db.Companys, "CompanyID", "CompanyCode", register.CompanyID);
            return View(register);
        }

        // GET: Registers/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Register register = await db.Registers.FindAsync(id);
            if (register == null)
            {
                return HttpNotFound();
            }
            return View(register);
        }

        // POST: Registers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Register register = await db.Registers.FindAsync(id);
            db.Registers.Remove(register);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public string CreateRandomPassword(int size)
        {
            string allowedChars = "";
            allowedChars += "1,2,3,4,5,6,7,8,9,0";
            char[] sep = { ',' };
            string[] arr = allowedChars.Split(sep);
            string passwordString = "";
            string password = "";
            string temp = "";
            Random rand = new Random();
            for (int i = 0; i < size; i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                passwordString += temp;
            }
            password = passwordString;
            return password;
        }
        public ActionResult Importexcel()
        {

            return View();

        }

        [HttpPost]
        public ActionResult Importexcel(HttpPostedFileBase file)
        {

            if (Request.Files["FileUpload"].ContentLength > 0)
            {
                string fileExtension =
                                     System.IO.Path.GetExtension(Request.Files["FileUpload"].FileName);

                if (fileExtension == ".xls" || fileExtension == ".xlsx")
                {

                    // Create a folder in App_Data named ExcelFiles because you need to save the file temporarily location and getting data from there. 
                    string fileLocation = Server.MapPath("~/App_Data/ExcelFiles/" + Request.Files["FileUpload"].FileName);
                  
                    if (System.IO.File.Exists(fileLocation))
                        System.IO.File.Delete(fileLocation);

                    Request.Files["FileUpload"].SaveAs(fileLocation);
                    string excelConnectionString = string.Empty;

                    excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation +
                                            ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    //connection String for xls file format.
                    if (fileExtension == ".xls")
                    {
                        excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileLocation +
                                                ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                    }
                    //connection String for xlsx file format.
                    else if (fileExtension == ".xlsx")
                    {

                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation +
                                                ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    }



                    //Create Connection to Excel work book and add oledb namespace
                    OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                    excelConnection.Open();
                    DataTable dt = new DataTable();

                    dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dt == null)
                    {
                        return null;
                    }

                    String[] excelSheets = new String[dt.Rows.Count];
                    int t = 0;
                    //excel data saves in temp file here.
                    foreach (DataRow row in dt.Rows)
                    {
                        excelSheets[t] = row["TABLE_NAME"].ToString();
                        t++;
                    }
                    OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);
                    DataSet ds = new DataSet();

                    string query = string.Format("Select * from [{0}]", excelSheets[1]);
                    using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                    {
                        dataAdapter.Fill(ds);
                    }
                    int id = Convert.ToInt32(System.Web.HttpContext.Current.Session["_CompanyRegID"].ToString());
                    int id2 = Convert.ToInt32(System.Web.HttpContext.Current.Session["_AGMID"].ToString());

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {

                        Register model = new Register();
                        model.memberno = ds.Tables[0].Rows[i]["Member Number"].ToString();
                        model.surnamecompany = ds.Tables[0].Rows[i]["surname"].ToString();
                        model.forenames = ds.Tables[0].Rows[i]["forenames"].ToString();
                        model.address = ds.Tables[0].Rows[i]["address"].ToString();
                        model.Balance = Convert.ToInt32(ds.Tables[0].Rows[i]["Units"].ToString());
                        model.membertype = "Physical";
                        model.DateAdded = DateTime.Now;
                        model.AGMID = id2;
                        model.CompanyID = id;
                        model.regstatus = "Activate";

                        var reg = db.Registers.ToList().Where(a => a.memberno == model.memberno);
                      string mem = "";
                        foreach(var row in reg)
                        {
                            mem = row.memberno;
                        }
                        // SAVE THE DATA INTO DATABASE HERE. I HAVE USED WEB API HERE TO SAVE THE DATA. 
                        // YOU CAN USE YOUR OWN PROCESS TO SAVE.
                        if (mem != model.memberno)
                        {
                            string cs = ConfigurationManager.ConnectionStrings["DataContext"].ConnectionString;

                            using (SqlConnection cn = new SqlConnection(cs))
                            {
                                string sql =
                                    "INSERT INTO Registers(memberno,surnamecompany,forenames,address,Balance,membertype,DateAdded,AGMID,CompanyID,regstatus) VALUES (@memberno,@surnamecompany,@forenames,@address,@Balance,@membertype,@DateAdded,@AGMID,@CompanyID,@regstatus)";
                                SqlCommand cmd = new SqlCommand(sql);
                                cmd.CommandType = CommandType.Text;
                                cmd.Connection = cn;

                                cmd.Parameters.AddWithValue("@memberno", model.memberno);
                                cmd.Parameters.AddWithValue("@surnamecompany", model.surnamecompany);
                                cmd.Parameters.AddWithValue("@forenames", model.forenames);
                                cmd.Parameters.AddWithValue("@address", model.address);
                                cmd.Parameters.AddWithValue("@Balance", model.Balance);
                                cmd.Parameters.AddWithValue("@membertype", model.membertype);
                                cmd.Parameters.AddWithValue("@DateAdded", model.DateAdded);
                                cmd.Parameters.AddWithValue("@AGMID", model.AGMID);
                                cmd.Parameters.AddWithValue("@CompanyID", model.CompanyID);
                                cmd.Parameters.AddWithValue("@regstatus", model.regstatus);

                                cn.Open();
                                try
                                {
                                    cmd.ExecuteNonQuery();
                                    cn.Close();
                                }
                                catch (Exception)
                                {

                                    throw;
                                }

                            }
                        }
                     
                 
                    }
                    
                       
                 
                }
                
                else
                {
                    ModelState.AddModelError("", "Please select a register to upload Excel File.");
                }
            }
         // System.IO.File.Delete(Server.MapPath("~/App_Data/ExcelFiles/" + Request.Files["FileUpload"].FileName));
            return RedirectToAction("Index");
           
    }
}
}
