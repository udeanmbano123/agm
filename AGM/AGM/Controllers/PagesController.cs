﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AGM.DAL;
using AGM.Models;

namespace AGM.Controllers
{
    public class PagesController : Controller
    {
        private DataContext db = new DataContext();

        // GET: Pages
        public async Task<ActionResult> Index()
        {
            return View(await db.Pages.ToListAsync());
        }

        // GET: Pages/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Page page = await db.Pages.FindAsync(id);
            if (page == null)
            {
                return HttpNotFound();
            }
            return View(page);
        }

        // GET: Pages/Create
        public ActionResult Create()
        {
            var list = db.Roles.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist = new List<SelectListItem>();
            selectlist.Add(new SelectListItem() { Text = "", Value = "" });
            foreach (var row in list)
            {
                //Adding every record to list  
                selectlist.Add(new SelectListItem { Text = row.RoleName, Value = row.RoleName.ToString() });
            }
            var list2 = db.Categorys.ToList().Where(a => a.CategoryName == Request["assignedRole"]);
            //Create List of SelectListItem
            List<SelectListItem> selectlist2 = new List<SelectListItem>();
            selectlist2.Add(new SelectListItem() { Text = Request[""], Value = Request[""] });
            foreach (var row in list2)
            {
                //Adding every record to list  
                selectlist2.Add(new SelectListItem { Text = row.CategoryName, Value = row.CategoryName.ToString() });
            }

            ViewBag.Category = selectlist2;
            ViewBag.Role = selectlist;
            return View();
        }

        // POST: Pages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "PageID,ControllerName,ViewName,assignedRole,Category")] Page page)
        {
            if (ModelState.IsValid)
            {
                db.Pages.Add(page);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            var list = db.Roles.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist = new List<SelectListItem>();
            selectlist.Add(new SelectListItem() { Text = Request["assignedRole"], Value = Request["assignedRole"] });
            foreach (var row in list)
            {
                //Adding every record to list  
                selectlist.Add(new SelectListItem { Text = row.RoleName, Value = row.RoleName.ToString() });
            }
            var list2 = db.Categorys.Include(m => m.Roles).ToList().Where(m => m.Roles.RoleName == Request["assignedRole"]);
            //Create List of SelectListItem
            List<SelectListItem> selectlist2 = new List<SelectListItem>();
            selectlist2.Add(new SelectListItem() { Text = Request[""], Value = Request[""] });
            foreach (var row in list2)
            {
                //Adding every record to list  
                selectlist2.Add(new SelectListItem { Text = row.CategoryName, Value = row.CategoryName.ToString() });
            }
            ViewBag.Role = selectlist;
            ViewBag.Category = selectlist2;
            return View(page);
        }

        // GET: Pages/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Page page = await db.Pages.FindAsync(id);
            if (page == null)
            {
                return HttpNotFound();
            }

            var list = db.Roles.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist = new List<SelectListItem>();
            selectlist.Add(new SelectListItem() { Text = page.assignedRole, Value =page.assignedRole});
            foreach (var row in list)
            {
                //Adding every record to list  
                selectlist.Add(new SelectListItem { Text = row.RoleName, Value = row.RoleName.ToString() });
            }
            var list2 = db.Categorys.ToList().Where(a => a.CategoryName == Request["assignedRole"]);
            //Create List of SelectListItem
            List<SelectListItem> selectlist2 = new List<SelectListItem>();
            selectlist2.Add(new SelectListItem() { Text = page.Category, Value = page.Category });
            foreach (var row in list2)
            {
                //Adding every record to list  
                selectlist2.Add(new SelectListItem { Text = row.CategoryName, Value = row.CategoryName.ToString() });
            }

            ViewBag.Category = selectlist2;
            ViewBag.Role = selectlist;
            return View(page);
        }

        // POST: Pages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "PageID,ControllerName,ViewName,assignedRole,Category")] Page page)
        {
            if (ModelState.IsValid)
            {
                db.Entry(page).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            var list = db.Roles.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist = new List<SelectListItem>();
            selectlist.Add(new SelectListItem() { Text = Request["assignedRole"], Value = Request["assignedRole"] });
            foreach (var row in list)
            {
                //Adding every record to list  
                selectlist.Add(new SelectListItem { Text = row.RoleName, Value = row.RoleName.ToString() });
            }
            var list2 = db.Categorys.Include(m => m.Roles).ToList().Where(m => m.Roles.RoleName == Request["assignedRole"]);
            //Create List of SelectListItem
            List<SelectListItem> selectlist2 = new List<SelectListItem>();
            selectlist2.Add(new SelectListItem() { Text = Request[""], Value = Request[""] });
            foreach (var row in list2)
            {
                //Adding every record to list  
                selectlist2.Add(new SelectListItem { Text = row.CategoryName, Value = row.CategoryName.ToString() });
            }
            ViewBag.Role = selectlist;
            ViewBag.Category = selectlist2;
            return View(page);
        }

        // GET: Pages/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Page page = await db.Pages.FindAsync(id);
            if (page == null)
            {
                return HttpNotFound();
            }
            return View(page);
        }

        // POST: Pages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Page page = await db.Pages.FindAsync(id);
            db.Pages.Remove(page);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
