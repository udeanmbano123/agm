﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using Newtonsoft.Json;
using AGM.DAL.Security;
using TrackerEnabledDbContext.Common.Configuration;

namespace AGM
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            GlobalTrackingConfig.DisconnectedContext = true;
            //   SqlDependency.Start(connString);
        }

        protected void Session_Start(Object sender, EventArgs e)
        {
            string meeting = "No activated Companies";
            int meetID = 0;
            string status = "No";
            HttpContext.Current.Session.Add("_CompanyID", meeting);
            HttpContext.Current.Session.Add("_CompanyName", meetID);
            HttpContext.Current.Session.Add("_Status", status);
            HttpContext.Current.Session.Add("_CompanyRegID", meeting);
            HttpContext.Current.Session.Add("_AGMID", meeting);
            HttpContext.Current.Session.Add("SearchID", meeting);
            HttpContext.Current.Session.Add("SearchID2", meeting);
        }

        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {

                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);

                CustomPrincipalSerializeModel serializeModel = JsonConvert.DeserializeObject<CustomPrincipalSerializeModel>(authTicket.UserData);
                CustomPrincipal newUser = new CustomPrincipal(authTicket.Name);
                newUser.UserId = serializeModel.UserId;
                newUser.FirstName = serializeModel.FirstName;
                newUser.LastName = serializeModel.LastName;
                newUser.roles = serializeModel.roles;

                HttpContext.Current.User = newUser;
            }

        }
    }
}
