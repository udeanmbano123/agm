﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using AGM.DAL;
using AGM.Models;
using EBoard.Models;



namespace AGM.Repositories
{
    public class ContentRepository
    {
        private readonly DataContext db = new DataContext();
        public int UploadImageInDataBase(HttpPostedFileBase file, ContentViewModel contentViewModel,int fID)
        {
            contentViewModel.Image = ConvertToBytes(file);
            contentViewModel.UserID = fID;
            var Content = new Models.Content
            {
                Title = contentViewModel.Title,
                UserID = contentViewModel.UserID,
                 Image = contentViewModel.Image
            };
             db.Contents.Add(Content);
            int i = db.SaveChanges();
            if (i == 1)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }

        public byte[] ConvertToBytes(HttpPostedFileBase image)
        {
            byte[] imageBytes = null;
            BinaryReader reader = new BinaryReader(image.InputStream);
            imageBytes = reader.ReadBytes((int)image.ContentLength);
            return imageBytes;
        }
    }
}