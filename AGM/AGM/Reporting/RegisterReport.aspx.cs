﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;

namespace AGM.Reporting
{
    public partial class RegisterReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var ReportName = ConfigurationManager.AppSettings["CrystalReportFilePath"];
               

             //  ReportDocument cryRpt = new ReportDocument();
            //   cryRpt.Load(Server.MapPath("~/Reporting/rptAccountsCreated.rpt"));
       // cryRpt.SetParameterValue("SearchID",HttpContext.Current.Session["SearchID"]);
         //      cryRpt.SetParameterValue("SearchID2",HttpContext.Current.Session["SearchID2"]);
              // CrystalReportViewer1.ReportSource = cryRpt;
      
            ReportDocument cryRpt = new ReportDocument();
            cryRpt.Load(Server.MapPath("CrystalReport1.rpt"));
            cryRpt.SetParameterValue("SearchID",HttpContext.Current.Session["SearchID"]);
           cryRpt.SetParameterValue("SearchID2",HttpContext.Current.Session["SearchID2"]);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            cryRpt.ExportToDisk(ExportFormatType.PortableDocFormat, "ReportRegister.rpt");
            cryRpt.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "ReportRegister");
            Response.Redirect("~/Report/RegisterReport");
            if (!IsPostBack)
            {
                if (CrystalReportViewer1 != null)
                    CrystalReportViewer1.Dispose();
                CrystalReportViewer1 = null;

                if (cryRpt != null)
                {
                    cryRpt.Close();
                    cryRpt.Dispose();
                }
                cryRpt = null;

                GC.Collect();
            }
            else
            {
                
            }
        }
    }
}