﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using AGM.Models;
using EBoard.Models;
using TrackerEnabledDbContext.Identity;

namespace AGM.DAL
{
    public class DataContext : TrackerIdentityContext<ApplicationUser>
    {
        public DataContext()
            : base("DataContext")
        {}
        public DbSet<Department> Departments { get; set; }
        public DbSet<Register> Registers { get; set; }
        public DbSet<Company> Companys { get; set; }
        public DbSet<Category> Categorys { get; set; }
        public DbSet<Models.AGM> AGMs { get; set; }
        public DbSet<Page> Pages { get; set; }
             public DbSet<Content> Contents { get; set; }
        public DbSet<Messages> Messagess { get; set; }
        public System.Data.Entity.DbSet<AGM.Models.FileUploadMinutes> FileUploadMinutess { get; set; }
        // public System.Data.Entity.DbSet<AGM.Models.ApplicationUser> ApplicationUsers { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder); // I had removed this
                                                /// Rest of on model creating here.
            modelBuilder.Entity<User>()
                .HasMany(u => u.Roles)
                .WithMany(r=>r.Users)
                .Map(m =>
                {
                    m.ToTable("UserRoles");
                    m.MapLeftKey("UserId");
                    m.MapRightKey("RoleId");
                });
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
    }
}