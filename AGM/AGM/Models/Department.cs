﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGM.Models
{
    public class Department
    {
        public int DepartmentID { get; set; }
        [Display(Name = "Department Code")]
        [Required(ErrorMessage = "Department code is required")]
        public string DepartmentCode { get; set; }
        [Display(Name = "Department Name")]
        [Required(ErrorMessage = "Department Name is required")]
        public string DepartmentName { get; set; }
        [Display(Name = "Department Code")]
        [Required(ErrorMessage = "Department Head is required")]
        public string DepartmentHead{ get; set; }
     }
}
