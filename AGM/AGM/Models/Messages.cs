﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EBoard.Models
{
    public class Messages
    {   [Key]
        public int MessageID { get; set; }
        [Display(Name = "Add Feed")]
        [Required]
        public string Message { get; set; }
       public string EmptyMessage { get; set; }
        public DateTime MessageDate { get; set; }
        public int CompanyID { get; set; }
        public string Status { get; set; }
    }
}
