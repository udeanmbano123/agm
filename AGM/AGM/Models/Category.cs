﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGM.Models
{
    public class Category
    {   [Key]
        public int CategoryID { get; set; }
        [Required]
        public string CategoryName { get; set; }
        [Required]
        public int? RoleID { get; set; }
        [ForeignKey("RoleID")]
        public virtual Role Roles { get; set; }
    }
}
