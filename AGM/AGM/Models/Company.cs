﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGM.Models
{
    public class Company
    {
        [Key]
        public int CompanyID { get; set; }
        public string CompanyCode { get; set; }
        [Required]
        public string CompanyName { get; set; }
        [Required]
        public string ISIN { get;set; }
        [Required]
        public string CompanyClassification { get; set; }
        [Required]
        public string Address { get; set; }

        public string Telephone { get; set; }
        public string Mobile{ get; set; }
        [Required(ErrorMessage = "eMail is required")]
        [StringLength(35, ErrorMessage = "eMail Length Should be less than 35")]
        [RegularExpression(@"^[\w-]+(\.[\w-]+)*@([a-z0-9-]+(\.[a-z0-9-]+)*?\.[a-z]{2,6}|(\d{1,3}\.){3}\d{1,3})(:\d{4})?$", ErrorMessage = "eMail is not in proper format")]
        [DataType(DataType.EmailAddress)]
        [DisplayName("EmailAddress")]
        public string Email { get; set; }
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MMM/yyyy}")]
        public DateTime DateAdded { get; set; }
        [DataType(DataType.Url)]
        public string Website { get; set; }
        public Boolean ActivationStatus { get; set; }


    }
}
