﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AGM.Models
{
    public class Content 
    {     [Key]
        public int ID { get; set; }
        public string Title { get; set; }

        public byte[] Image { get; set; }
        public int UserID { get; set; }
         [ForeignKey("UserID")]
        public virtual User Users { get; set; }
    }
}