﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGM.Models
{
    public class Register
    {
        public int RegisterID { get; set; }
        //AGMID,COMPANY CODE
        public string memberno { get; set; }
        public string regstatus { get; set; }
        [Required]
        public string membertype { get; set; }
        [Required]
        [Display(Name = "Forenames")]
        public string forenames { get; set; }
        [Required]
        [Display(Name="Surname/Company")]
        public string surnamecompany { get; set; }
     public int Balance { get; set; }
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MMM/yyyy}")]
        public DateTime DateAdded { get; set; }
        public int CompanyID { get; set; }
        public string address { get; set; }
        [ForeignKey("CompanyID")]
        public virtual Company Company { get; set; }
        public int AGMID { get; set; }
        [ForeignKey("AGMID")]
        public virtual AGM AGM { get; set; }
    }
}
