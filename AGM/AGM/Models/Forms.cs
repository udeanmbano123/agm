﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AGM.Models
{
    public class Page
    {
        public int PageID { get; set; }
        [Required]
        [DisplayName("ControllerName")]
        public string ControllerName { get; set; }
        [Required]
        [DisplayName("ActionName")]
        public string ViewName { get; set; }
        [Required]
        [DisplayName("AssignedToRole")]
        public string assignedRole { get; set; }
        [Required]
        [DisplayName("Category Name")]
        public string Category { get; set; }
    }
}