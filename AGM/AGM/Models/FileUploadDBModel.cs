﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AGM.Models
{
    public class MyViewModel
    {
        [DisplayName("Select Files to Upload")]
        [Required]
        public IEnumerable<HttpPostedFileBase> File { get; set; }
    }

    public class FileUploadMinutes
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public byte[] File { get; set; }
         public int? DocMeetID { get; set; }
        //[ForeignKey("DocMeetID")]
       // public virtual Meeting Meeting { get; set; }
    }
}

